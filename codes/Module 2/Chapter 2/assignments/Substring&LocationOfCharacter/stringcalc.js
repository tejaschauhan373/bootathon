function convert() {
    var str1 = document.getElementById("str1");
    var char = document.getElementById("char");
    var string1 = str1.value;
    var char1 = char.value;
    document.getElementById("display").innerHTML = "<b>The original string :</b> " + string1;
    var string2 = string1.substring(Math.floor(string1.length / 2), string1.length);
    document.getElementById("display").innerHTML += "<br><b>Substring :</b>" + string2;
    var index = string1.indexOf(char1);
    if (index == -1) {
        alert("character " + char1 + " is not a part of the string");
    }
    else {
        document.getElementById("display").innerHTML += "<br><b>Character</b> " + char1 + " <b>is at the position : </b>" + (index + 1);
    }
}
//# sourceMappingURL=stringcalc.js.map