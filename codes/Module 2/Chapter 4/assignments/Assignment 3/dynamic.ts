function dynamic(): void
{
    /**
     *  This function is used for create dynamic table generation for multiplication table of 
     *  any number and table should be from 1 to entered number. For example :
     *  When user will enter number as 6 then table will start from 6 * 1 = 6 and it will end at 6 * 6 = 36.
     */

    var input : HTMLInputElement = <HTMLInputElement>document.getElementById("input");
    var table : HTMLTableElement = <HTMLTableElement>document.getElementById("table_1");

    var num : number = +input.value; // covert string to float data type
    var count : number = 1;

    // check the value entered by user is numeric or not.
    // it is not numeric give alert to user to enter correct value.
    if(isNaN(num))
    {
        alert("Enter numeric value only.")
    }

    // delete the all row from past table if there are.
    while( table.rows.length > 1)
    {
        table.deleteRow(1);
    }
    
    // creating new row for new input
    for( count=1; count <=num ; count++ )
    {
        //create row in table
        var row : HTMLTableRowElement = table.insertRow();

        // create first cell element 
        //here  we have also enabled readonly attribute so user can not change values of input field.  
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "number";
        text.style.backgroundColor="yellow;"
        text.id = "t1"+count;
        text.readOnly=true;
        text.style.textAlign = "center";
        text.value = num.toString();
        cell.appendChild(text);
        
        // create second cell element 
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.readOnly=true;
        text.id = "t2"+count;
        text.style.backgroundColor="yellow;"
        text.style.textAlign = "center";
        text.value = "*";
        cell.appendChild(text);

        // create Third cell element 
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "number";
        text.id = "t3"+count;
        text.readOnly=true;
        text.style.backgroundColor="yellow;"
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        // create fourth cell element 
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "t4"+count;
        text.readOnly=true;
        text.style.backgroundColor="yellow;"
        text.style.textAlign = "center";
        text.value = "=";
        cell.appendChild(text);

        // create fifth cell element 
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "number";
        text.readOnly=true;
        text.id = "t5"+count;
        text.style.textAlign = "center";
        text.style.backgroundColor="yellow;"
        console.log(num);
        console.log(count)
        text.value = (count*num).toString();  // perform multiplication, convert into string and assign it.
        cell.appendChild(text);
    }

}