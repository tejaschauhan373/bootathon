function dynamic(): void
{    
    var input : HTMLInputElement = <HTMLInputElement>document.getElementById("input");
    var table : HTMLTableElement = <HTMLTableElement>document.getElementById("table_1");

    var num : number = +input.value; // covert string to float data type
    var count : number = 1;

    // delete the all row from past table if there are.
    while( table.rows.length > 1)
    {
        table.deleteRow(1);
    }
    
    // creating new row for new input
    for( count=1; count <=num ; count++ )
    {
        //create row in table
        var row : HTMLTableRowElement = table.insertRow();

        // create first cell element 
        //here  we have also enabled readonly attribute so user can not change values of input field.  
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "number";
        text.style.backgroundColor="yellow;"
        text.id = "t1"+count;
        text.readOnly=true;
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        
        // create second cell element 
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "number";
        text.readOnly=true;
        text.id = "tt"+count;
        text.style.backgroundColor="yellow;"
        text.style.textAlign = "center";
        text.value = (count*count).toString();;
        cell.appendChild(text);

       
    }

}