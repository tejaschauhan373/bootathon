function factorial() {
    var input = document.getElementById("input_text");
    var output = document.getElementById("result");
    var num = +input.value;
    var count = 1;
    var sum = 1;
    while (count <= sum) {
        sum += sum * count;
        count++;
    }
    output.value = sum.toString();
}
//# sourceMappingURL=factorial_script.js.map